//some useful functions like working with files or random
#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <string>


//getting OS type

#if defined(_WIN32) ||defined(_WIN64)
#define OS_WINDOWS
#else
#define OS_UNIX
#endif


//#define OS_UNIX
//#define OS_WINDOWS

#ifdef OS_WINDOWS

#include <minmax.h>
#include <Windows.h>

#endif

#include <opencv2/opencv.hpp>




#ifdef OS_UNIX

#define min(a, b) fmin((a),  (b))
#define max(a, b) fmax((a),  (b))

#else

#define fmin(a, b) min((a),  (b))
#define fmax(a, b) max((a),  (b))

#endif






#define INF std::numeric_limits<float>::infinity()

template <typename T>
T sqr(const T &x) {
    return x * x;
}

//double sqr(double x);



//from -val + center to val + center
//uniform distribution
float random(float val, float center = 0);

//from 0 to val - 1
int irand(int val);


bool FindFilesAndFolders(const std::string& sPath, std::vector<std::string> &vFiles, std::vector<std::string> &&vDirectories = std::vector<std::string>(), bool bRec = false);


void mkdir(const std::string &path, bool removeIfExists = false);

std::vector<std::string> split(const std::string &s, char delim, std::vector<std::string> &elems);

std::vector<std::string> split(const std::string &s, char delim = ' ');

std::string merge(const std::vector<std::string> &v, char delim = ' ');

//parses file name, vPath in decrease order
void parseFileName(const std::string &s, std::string &sExpansion, std::string &sName,
                   std::vector<std::string> &&vPath = std::vector<std::string>());

//corrects '/' and '\'
std::string correctAdress(const std::string &s);

//changes spaces ' ' to '_'
std::string changeSpaces(const std::string &s);





//#ifdef OS_WINDOWS
//#if (_MSC_VER <= 1700) //elder then visual studio 2012

namespace std {
    
    template <typename T>
    std::string to_string(T value)
    {
        std::ostringstream os ;
        os << value ;
        return os.str() ;
    }
    
} //end of namespace std

//#endif
//#endif


#ifdef DEBUG

void test_output(const std::vector<float>& d,const std::string& FileName);

//comma separator
template <typename T>
struct comma_separator : std::numpunct<T> {
    typename std::numpunct<T>::char_type do_decimal_point() const{
        return ',';
    }
};

template <typename T>
std::basic_ostream<T>& comma_sep(std::basic_ostream<T>& os)
{
    os.imbue(std::locale(std::locale(""), new comma_separator<T>));
    return os;
}

#endif


float getTimeSec();





//=========================
//Logger
//=========================

#ifndef __FUNCSIG__
#define __FUNCSIG__
#endif


#ifdef PRINTF_DEBUG_INFO
#ifndef logf
#define logf(fmt, ...) printf(("%s \n%s:[Line %d] \n" fmt),__PRETTY_FUNCTION__,__FILE__,__LINE__,##__VA_ARGS__)
#endif
#endif

#ifdef __FUNCSIG__
#define __PRETTY_FUNCTION__ __FUNCSIG__
#define log_info __PRETTY_FUNCTION__<<'\n'<<__FILE__<<" [Line:"<<__LINE__<<"]\n"
#else
#define log_info __FILE__<<" [Line:"<<__LINE__<<"]\n"
#endif


class Logger {
    
    std::ofstream ofs;
    bool bCoutActive;
    bool bOfsActive;
    
public:
    
    Logger(const std::string& filename = "log.txt")
    : ofs(filename.c_str())
    {
        bCoutActive = true;
        bOfsActive = true;
        //std::clog.rdbuf(ofs.rdbuf());
    }
    
    // this is the type of logger
    typedef std::basic_ostream<char, std::char_traits<char> > CoutType;
    
    // this is the function signature of std::endl
    typedef CoutType& (*StandardEndLine)(CoutType&);
    
    // define an operator<< to take in std::endl
    Logger& operator<<(StandardEndLine manip) {
        // call the function, but we cannot return it's value
        if (bCoutActive) {
            manip(std::cout);
        }
        if (bOfsActive) {
            manip(ofs);
        }
        return *this;
    }
    
    void setCoutActive(bool bActive) {
        bCoutActive = bActive;
    }
    
    void setOfsActive(bool bActive) {
        bOfsActive = bActive;
    }
    
    bool isCoutActive()const  {
        return bCoutActive;
    }
    
    bool isOfsActive()const {
        return bOfsActive;
    }
    
    std::ofstream& getOfs() {
        return ofs;
    }
    
};

template<typename To, typename From> To convert(From f);

template <typename T>
Logger& operator<<(Logger & stream, T const & x) {
    //decltype - type of variable (for convertion)
    //typeid - id in int of the type
    /*if (typeid(x) == typeid(std::string())) {
     //std::string s = <decltype(std::string())>(x);
     std::string s = convert<std::string>(x);
     s = merge(split(x, '\t'), ' ');
     std::cout << s;
     stream.getOfs() << s;
     } else { 	  */
    if (stream.isCoutActive()) {
        std::cout << x;
    }
    if (stream.isOfsActive()) {
        stream.getOfs() << x;
    }
    //}
    return stream;
}

extern Logger logger;











//==============================
//opencv
//==============================



float dist2(const cv::Point2f &p1, const cv::Point2f &p2);

//float dist2(const cv::Point2f &p1, const cv::Point2f &p2);

cv::Mat resize(const cv::Mat &mSrc, float newWidth, bool bInterpolation = true);

cv::Mat rotateMat(cv::Mat mSrc, double angle);

void imshow(const cv::Mat &mSrc);




//===========================
//math opencv
//===========================

#ifndef M_PI

#undef M_PI
#define M_PI 3.14159265359

#endif


cv::Vec2i vec2i(const cv::Point2f & start, const cv::Point2f &end);

float calcLength(const cv::Vec2i &v);

//from 0 to PI
float angleBetweenVectors(const cv::Vec2i &v1, const cv::Vec2i &v2);

void linesIntersection(const cv::Point2f & p1, const cv::Point2f &p2, const cv::Point2f &p3, const cv::Point2f &p4, cv::Point2f &p);

bool segmentsIntersection(const cv::Point2f &a1, const cv::Point2f &a2, const cv::Point2f &b1, const cv::Point2f &b2, cv::Point2f &p);

float calcMaxDistance(const std::vector<cv::Point2f> &v);

cv::Point2f rotatePoint(const cv::Point2f& inPoint, const cv::Point2f& center, const double& angRad);

float distFromPointToLine(const cv::Point &start, const cv::Point &end, const cv::Point &p);

cv::Point symmetricPoint(const cv::Point &start, const cv::Point &end, const cv::Point &p);



