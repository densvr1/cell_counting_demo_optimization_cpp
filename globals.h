#pragma once


const std::string DATASET_PATH = "..\\datasets\\3K_VehicleDetection_dataset\\Train\\"; 
const std::string SAMPLES_PATH = "Z:\\Documents\\work\\2016_01_VehicleDetection\\VehicleDetection\\output\\haar_samples\\";

const std::string OPENCV_DIR = "C:\\mydev\\opencv300\\opencv\\build\\x64\\vc12\\bin\\";

//const std::string DATASET_TEST_PATH = "..\\datasets\\3K_VehicleDetection_dataset\\Test\\";
const std::string DATASET_TEST_PATH = "../datasets/testvid/";

