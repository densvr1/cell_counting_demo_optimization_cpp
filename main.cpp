#include "useful.h"
#include "Optimization.h"
#include <map>

int SHOW_IMAGE_WIDTH = 500;




void imshow(const std::string &name, const cv::Mat &imgMat) {
    cv::imshow(name, resize(imgMat, SHOW_IMAGE_WIDTH));
}

void binarizeInRange(cv::Mat src, cv::Mat &dst,
                     int r0, int g0, int b0, int r1, int g1, int b1);

double compareWithMask(cv::Mat gray, cv::Mat mask);

cv::Point computeCentroid(cv::InputArray mask);



void processImage(const std::string &inputImageName);


int main(int argc, char** argv) {



    const std::string inputImageName = "53_x20_10 1 4 пассаж";


    //prepare mask
    cv::Mat mask = cv::imread(inputImageName + "_mask.jpeg", cv::IMREAD_COLOR);
    cv::Mat healthyMask = cv::Mat();
    cv::Mat infectedMask = cv::Mat();
    cv::inRange(mask, cv::Scalar(0, 200, 200), cv::Scalar(50, 255, 255), healthyMask); //BGR yellow
    cv::inRange(mask, cv::Scalar(200, 200, 200), cv::Scalar(255, 255, 255), infectedMask); //BGR white

    //imshow("healthy", healthyMask);
    //imshow("infected", infectedMask);

    cv::Mat cellMask = healthyMask + infectedMask;
    imshow("cellMask", cellMask);




    cv::Mat imageMat = cv::imread(inputImageName + ".jpeg", cv::IMREAD_COLOR);


    //imshow("src", resize(imageMat, SHOW_IMAGE_WIDTH));

/*
    //minK	2.92255	optimalBounds	0 0 0 53 23 51   //cell mask
    //minK	1.2533	optimalBounds	0 0 0 255 34 57  //infected mask

    //optimization
    double optimalK = +INF; //worst case
    std::vector<int> optimalBounds(6, 0);
    while(true) {
        double curBestK = optimalK;
        for (int i = 0; i < 6; i++) {
            std::vector<int> bounds = optimalBounds;
            for(short diff = -100; diff < 100; diff++) {
                cv::Mat binMat;
                bounds[i] += diff;
                if (bounds[i] < 0 || bounds[i] > 255) {
                    bounds[i] -= diff;
                    continue;
                }
                binarizeInRange(imageMat, binMat, bounds[0], bounds[1], bounds[2], bounds[3], bounds[4], bounds[5]);
                double k = compareWithMask(binMat, infectedMask);
                if (k < optimalK) {
                    optimalK = k;
                    optimalBounds[i] = bounds[i];
                    //imshow("bin", binMat);
                    //cv::waitKey(0);
                }
                bounds[i] -= diff;
            }
        }
        logger << "minK" << "\t" << optimalK << "\t" << "optimalBounds" << "\t";
        for (const auto &ii: optimalBounds) {
            logger << ii << ' ';
        }
        logger << std::endl;
        if (optimalK >= curBestK) {

            break;
        }
    }
    return 0;

*/

/*
    for (int i = 0; )
        for (int r0 = 0; r0 < 10; r0 += 10) {
            for (int g0 = 0; g0 < 10; g0 += 10) {
                for (int b0 = 0; b0 < 10; b0 += 10) {
                    for (int r1 = r0 + 51; r1 <; r1 += 10) {
                        for (int g1 = g0 + 31; g1 < 20; g1 += 10) {
                            for (int b1 = b0 + 51; b1 < 256; b1 += 10) {
                                cv::Mat binMat;
                                binarizeInRange(imageMat, binMat, r0, g0, b0, r1, g1, b1);
                                double k = compareWithMask(binMat, cellMask);
                                if (k < minK) {
                                    minK = k;
                                    optimalBounds[0] = r0;
                                    optimalBounds[1] = g0;
                                    optimalBounds[2] = b0;
                                    optimalBounds[3] = r1;
                                    optimalBounds[4] = g1;
                                    optimalBounds[5] = b1;
                                }
                                imshow("bin", binMat);
                                cv::waitKey(0);
                                logger << "minK" << "\t" << minK << "\t" << "optimalBounds" << "\t";
                                for (const auto &i: optimalBounds) {
                                    logger << i << ' ';
                                }
                                logger << std::endl;
                            }
                        }
                    }
                }
            }
        }
*/



    std::vector<std::string> files;

    FindFilesAndFolders("/Users/densvr/Desktop/HumanDetection/data/1part", files, std::vector<std::string>(), true);


    std::vector<std::string> filesToProcess;
    for(int i = 0; i < files.size(); i++) {
        std::string fileName = files[i];
        std::string expansion, name;
        parseFileName(fileName, expansion, name, std::vector<std::string>());
        if (!(expansion == "jpg" || expansion == "JPG" ||
            expansion == "jpeg" || expansion == "JPEG" ||
            expansion == "png" || expansion == "PNG")) {
            continue;
        }
        if (fileName.find("processed.jpeg") != -1) {
            continue;
        }
        filesToProcess.push_back(files[i]);
       // processImage(fileName);
    }



    ImageProcessorFuncVec imageProcessorFuncVec(files, std::map<std::string, int>());

    std::vector<float> x = { 0, 0, 0, 53, 23, 51 };
    std::vector<float> r = { 10, 10, 10, 10, 10, 10 };
    std::vector<float> minX = { 0, 0, 0, 0, 0, 0 };
    std::vector<float> maxX = { 255, 255, 255, 255, 255, 255 };
    std::vector<size_t> internalSteps = std::vector(x.size(), (size_t)10);

    calcOptimumVec(imageProcessorFuncVec, x, r, minX, maxX, internalSteps, true, 100);


    return 0;
}

class ImageProcessorFuncVec: public FuncVec {

private:
    std::vector<std::string> files;
    std::map<std::string, int> labelledCounts;

public:
    ImageProcessorFuncVec(const std::vector<std::string> &files, const std::map<std::string, int> &labelledCounts) {
        this->files = files;
        this->labelledCounts = labelledCounts;
    }

    float operator()(const std::vector<float> &x) {

        int samplesCount = 0;

        float sum = 0;
        for(int i = 0; i < files.size(); i++) {
            int processedCount = processImage(files[i], x);
            if (labelledCounts.find(files[i]) == files.end()) {
                continue;
            }
            samplesCount++;
            int labelledCount = labelledCounts[files[i]];
            sum += min(labelledCount, processedCount) / (float)max(labelledCount, processedCount);
        }

        return sum / samplesCount * 100;
    }

};


int processImage(const std::string &inputImageName, const std::vector<float> &params) {

    cv::Mat imageMat = cv::imread(inputImageName, cv::IMREAD_COLOR);


    cv::Mat binMat;
    binarizeInRange(imageMat, binMat,
                    (int)params[0], (int)params[1], (int)params[2], (int)params[3], (int)params[4], (int)params[5]);

    //imshow("bin", binMat);
    //cv::waitKey(0);

    cv::Mat cannyMat = binMat;
    ///cv::Canny(binMat, cannyMat, 100, 200);


    cv::Mat structureElementMat = cv::getStructuringElement(cv::MorphShapes::MORPH_ELLIPSE, cv::Size(3, 3));
    //cv::dilate(cannyMat, cannyMat, structureElementMat, cv::Point(), 1);
    cv::erode(cannyMat, cannyMat, structureElementMat, cv::Point(), 2);

    //cv::imshow("canny", resize(cannyMat, SHOW_IMAGE_WIDTH));
    //cv::waitKey();



    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(cannyMat, contours, cv::RetrievalModes::RETR_LIST, cv::ContourApproximationModes::CHAIN_APPROX_SIMPLE);


    cv::Mat binInfectedMat;
    binarizeInRange(imageMat, binInfectedMat, 0, 0, 0, 255, 34, 57);

    int healthyCount = 0;
    int infectedCount = 0;
    cv::Mat contourMat = cv::Mat::zeros(cv::Size(imageMat.cols, imageMat.rows), CV_8UC1);
    for(int i = 0; i < contours.size(); i++) {
        std::vector<cv::Point> &contour = contours[i];

        double contourArea = cv::contourArea(contour);
        if (contourArea < 10 * 10) {
            continue;
        }

        contourMat.setTo(0);
        std::vector<std::vector<cv::Point>> contourVec;
        contourVec.push_back(contour);
        cv::drawContours(contourMat, contours, i, 255, -1);

        double infectedRatio = 0;
        for(int x = 0; x < contourMat.cols; x++) {
            for(int y = 0; y < contourMat.rows; y++) {
                if (contourMat.at<uchar>(x, y) == 0) {
                    continue;
                }
                if (binInfectedMat.at<uchar>(x, y) > 0) {
                    infectedRatio += 1;
                }
            }
        }
        infectedRatio /= contourArea;
        logger<<infectedRatio<<std::endl;
        bool isInfected = infectedRatio > 0.9;


        for(int j = 0; j < contour.size(); j++) {
            if (!isInfected) {
                cv::line(imageMat, contour[j], contour[(j + 1) % contour.size()],
                         cv::Scalar(0, 255, 0), 2);
            } else {
                cv::line(imageMat, contour[j], contour[(j + 1) % contour.size()],
                         cv::Scalar(0, 0, 255), 2);
            }

        }

        if (isInfected) {
            infectedCount++;
        } else {
            healthyCount++;
        }

        cv::Point center = computeCentroid(contour);
        if (isInfected) {
            cv::putText(imageMat, std::to_string(infectedCount), center, 1, 1.5, cv::Scalar(255, 0, 255), 2);
        } else {
            cv::putText(imageMat, std::to_string(healthyCount), center, 1, 1.5, cv::Scalar(255, 255, 0), 2);
        }
    }

    cv::putText(imageMat, std::to_string(healthyCount) + " healthy", cv::Point(5, imageMat.rows - 5), 1, 3, cv::Scalar(255, 255, 255), 2);
    cv::putText(imageMat, std::to_string(infectedCount) + " infected", cv::Point(5, imageMat.rows - 70), 1, 3, cv::Scalar(255, 255, 255), 2);


    cv::imwrite(inputImageName + "_processed.jpeg", imageMat);

    cv::imshow("res", resize(imageMat, SHOW_IMAGE_WIDTH));
    cv::waitKey();

    return healthyCount + infectedCount;

}


void binarizeInRange(cv::Mat src, cv::Mat &dst,
              int r0, int g0, int b0, int r1, int g1, int b1)
{
    cv::inRange(src, cv::Scalar(r0, g0, b0), cv::Scalar(r1, g1, b1), dst);
    dst = 255 - dst;
}

double compareWithMask(cv::Mat gray, cv::Mat mask) {
    cv::Mat diff32F;
    gray.convertTo(diff32F, CV_32FC1);
    cv::Mat mask32F;
    mask.convertTo(mask32F, CV_32FC1);
    diff32F = cv::abs(diff32F - mask32F);
    return cv::sum(diff32F)[0] / (gray.cols * gray.rows) /255.0 * 100;
}

cv::Point computeCentroid(cv::InputArray mask) {
    cv::Moments m = moments(mask, true);
    cv::Point center(m.m10/m.m00, m.m01/m.m00);
    return center;
}


