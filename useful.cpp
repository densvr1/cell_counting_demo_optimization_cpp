#include "useful.h"
#include <stdlib.h>


#ifdef OS_WINDOWS

#include <windows.h>
#include <time.h>

#else

#include <dirent.h>
#include <sys/stat.h>

#endif

#include <vector>
#include <string>
#include <algorithm>
#include <sstream>




using namespace std;

//=========================
//LOGGER
//=========================



Logger logger;


/*
 double sqr(double x) {
	return x * x;
 }  */



//from -val + center to val + center
//uniform distribution
float random(float val, float center) {
    return (( (rand()) % 10000) / 10000.0f * 2 - 1) * val + center;
}

//from 0 to val - 1
int irand(int val) {
    return (rand()) % val;
}


bool FindFilesAndFoldersRec(const string& sPath,vector<string> &vFiles,vector<string> &vDirectories, bool bRec);

bool FindFilesAndFolders(const string& sPath,vector<string> &vFiles,vector<string> &&vDirectories, bool bRec) {
    vFiles.clear();
    vDirectories.clear();
    
    bool res = FindFilesAndFoldersRec(sPath, vFiles, vDirectories, bRec);
    
    sort(vFiles.begin(),vFiles.end());
    sort(vDirectories.begin(),vDirectories.end());
    return res;
}



bool FindFilesAndFoldersRec(const string& sPath,vector<string> &vFiles,vector<string> &vDirectories, bool bRec) {
#ifdef OS_WINDOWS
    LPCTSTR lpszPath = sPath.c_str();
    
    TCHAR szFind[MAX_PATH];
    lstrcpy(szFind,lpszPath);
    lstrcat(szFind,"*.*");
    WIN32_FIND_DATA wfd;
    HANDLE hFind=FindFirstFile(szFind,&wfd);
    
    if(hFind==INVALID_HANDLE_VALUE) {
        return false;
    }
    do{
        if(lstrcmp(wfd.cFileName,".")==0||lstrcmp(wfd.cFileName,"..")==0) {
            continue;
        }
        char szFile[MAX_PATH];
        lstrcpy(szFile,lpszPath);
        lstrcat(szFile,wfd.cFileName);
        if((GetFileAttributes(szFile)&FILE_ATTRIBUTE_DIRECTORY)==FILE_ATTRIBUTE_DIRECTORY){
            std::string sDir = std::string(szFile) + "\\";
            vDirectories.push_back(sDir);
            if (bRec) {
                FindFilesAndFoldersRec(sDir, vFiles, vDirectories, true);
            }
        } else {
            vFiles.push_back(szFile);
        }
    } while (FindNextFile(hFind,&wfd));
    FindClose(hFind);
#else
    DIR *dir;
    class dirent *ent;
    class stat st;
    dir = opendir(sPath.c_str());
    if (dir == NULL) {
        return false;
    }
    while ((ent = readdir(dir)) != NULL) {
        const std::string fileName = ent->d_name;
        std::string fullFileName = sPath + "/" + fileName;
        if (fileName[0] == '.') {
            continue;
        }
        if (stat(fullFileName.c_str(), &st) == -1) {
            continue;
        }
        const bool isDirectory = (st.st_mode & S_IFDIR) != 0;
        if (isDirectory) {
            fullFileName += "/";
            vDirectories.push_back(fullFileName);
            if (bRec) {
                FindFilesAndFoldersRec(fullFileName, vFiles, vDirectories, true);
            }
        } else {
            vFiles.push_back(fullFileName);
        }
    }
    closedir(dir);
#endif
    return true;
}



void mkdir(const std::string &path, bool removeIfExists) {
    if (path == "") {
        return;
    }
    if (removeIfExists) {
        system(("rmdir " + path + " /s /q").c_str());
    }
    system(("mkdir " + path).c_str());
}


std::vector<std::string> split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

std::string merge(const std::vector<std::string> &v, char delim) {
    if (v.size() == 0) {
        return "";
    }
    std::string s = v[0];
    for(size_t i = 1; i < v.size(); i++) {
        s += delim + v[i];
    }
    return s;
}


//parses file name, vPath in decrease order
void parseFileName(const string &s, string &sExpansion, string &sName, vector<string> &&vPath) {
#ifdef OS_WINDOWS
    vector<string> v = split(s,'\\');
#else
    vector<string> v = split(s,'/');
#endif
    for(size_t i = 0; i < v.size() - 1; i++) {
        vPath.insert(vPath.begin(), v[i]);
    }
    v = split(*(v.end() - 1), '.');
    sName = v[0];
    if (v.size() > 1) {
        sExpansion = v[1];
    }
}

//corrects '/' and '\'
string correctAdress(const string &s) {
#ifdef OS_WINDOWS
    vector<string> v = split(s, 92);  // '\'
    string sRes = v[0];
    for(size_t i = 1; i < v.size(); i++) {
        sRes += "\\" + v[i];
    }
    return sRes;
#else
    vector<string> v = split(s, 92);  // '\'
    string sRes = v[0];
    for(size_t i = 1; i < v.size(); i++) {
        if (v[i].size() != 0) {
            sRes += "/" + v[i];
        }
    }
    sRes += "/";
    return sRes;
#endif
}

//changes spaces ' ' to '_'
string changeSpaces(const string &s) {
    vector<string> v = split(s,' ');
    string res;
    for(size_t i = 0; i < v.size(); i++) {
        if (i > 0) {
            res += "_";
        }
        res += v[i];
    }
    return res;
}

#ifdef DEBUG
void test_output(const vector<float>& d,const string& FileName) {
    FILE *fout;
    fout = fopen(FileName.c_str(),"wt");
    if (fout == NULL) {
        return;
    }
    for(size_t i = 0; i < d.size(); i++) {
        fprintf(fout,"%i\t%f\n",i,d[i]);
    }
    fclose(fout);
}
#endif

float getTimeSec() {
    /*
     __int64 cnt, freq;
     QueryPerformanceCounter((LARGE_INTEGER*)&cnt);
     QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
     
     return cnt / (float)freq;
     */
    return clock() / float(CLOCKS_PER_SEC);
}




//comma separator




//==============================
//opencv
//==============================

#include <opencv2/opencv.hpp>

float dist2(const cv::Point2f &p1, const cv::Point2f &p2) {
    return sqr(p1.x - p2.x) + sqr(p1.y - p2.y);
}

//float dist2(const cv::Point2f &p1, const cv::Point2f &p2) {
//	return sqr(float(p1.x - p2.x)) + sqr(float(p1.y - p2.y));
//}

cv::Mat resize(const cv::Mat &mSrc, float newWidth, bool bInterpolation) {
    cv::Mat mDst;
    int interpolation = bInterpolation ? 1 : 0;
    cv::resize(mSrc, mDst, cv::Size(int(newWidth), int(mSrc.rows * newWidth / (float)mSrc.cols)), interpolation);
    return mDst;
}

cv::Mat rotateMat(cv::Mat mSrc, double angle) {
    cv::Mat mDst;
    cv::Point2f pt(mSrc.cols / 2.0, mSrc.rows / 2.0);
    cv::Mat r = getRotationMatrix2D(pt, angle, 1.0);
    warpAffine(mSrc, mDst, r, cv::Size(mSrc.cols, mSrc.rows));
    return mDst;
}


#ifdef OS_WINDOWS

long imgNum = 0;
void imshow(const cv::Mat &mSrc) {
    mkdir("temp", false);
    std::stringstream ss;
    ss<<"temp\\"<<imgNum<<".jpg";
    std::string imgName;
    ss>>imgName;
    cv::imwrite(imgName, mSrc);
    system(imgName.c_str());
    imgNum++;
}

#endif





//===========================
//math opencv
//===========================

cv::Vec2i vec2i(const cv::Point2f & start, const cv::Point2f &end) {
    return cv::Vec2i(int(end.x - start.x), int(end.y - start.y));
}

float calcLength(const cv::Vec2i &v) {
    return sqrt(sqr(float(v[0])) + sqr(float(v[1])));
}

float angleBetweenVectors(const cv::Vec2i &v1, const cv::Vec2i &v2) {
    return acos( (v1[0] * v2[0] + v1[1] * v2[1]) /
                (sqrt(sqr((float)v1[0]) + sqr((float)v1[1])) * sqrt(sqr((float)v2[0]) + sqr((float)v2[1]))) );
}

void linesIntersection(const cv::Point2f & p1, const cv::Point2f &p2, const cv::Point2f &p3, const cv::Point2f &p4, cv::Point2f &p) {
    float x1 = (float)p1.x;
    float y1 = (float)p1.y;
    float x2 = (float)p2.x;
    float y2 = (float)p2.y;
    float x3 = (float)p3.x;
    float y3 = (float)p3.y;
    float x4 = (float)p4.x;
    float y4 = (float)p4.y;
    float x;
    float y;
    if (x2 == x1) {
        x2 += 0.1f;
    }
    if (x4 == x3) {
        x3 += 0.1f;
    }
    float k1 = (y2 - y1) / (x2 - x1);
    float k2 = (y4 - y3) / (x4 - x3);
    if (k1 == k2) {
        k1 += 0.01f;
    }
    x = (k1 * x1 - k2 * x3 + y3 - y1) / (k1 - k2);
    y = k1 * (x - x1) + y1;
    p.x = x;
    p.y = y;
}

bool segmentsIntersection(const cv::Point2f &a1, const cv::Point2f &a2, const cv::Point2f &b1, const cv::Point2f &b2, cv::Point2f &p) {
    
    float ax1 = a1.x;
    float ay1 = a1.y;
    float ax2 = a2.x;
    float ay2 = a2.y;
    float bx1 = b1.x;
    float by1 = b1.y;
    float bx2 = b2.x;
    float by2 = b2.y;
    
    
    float v1 = (bx2-bx1)*(ay1-by1)-(by2-by1)*(ax1-bx1);
    float v2 = (bx2-bx1)*(ay2-by1)-(by2-by1)*(ax2-bx1);
    float v3 = (ax2-ax1)*(by1-ay1)-(ay2-ay1)*(bx1-ax1);
    float v4 = (ax2-ax1)*(by2-ay1)-(ay2-ay1)*(bx2-ax1);
    if (v1 * v2 < 0 && v3 * v4 < 0) {
        linesIntersection(a1, a2, b1, b2, p);
        return true;
    }
    return false;
}

float calcMaxDistance(const std::vector<cv::Point2f> &v) {
    float maxDist2 = 0;
    for(size_t i = 0; i < v.size(); i++) {
        const cv::Point2f &p1 = v[i];
        for(size_t j = i + 1; j < v.size(); j++) {
            float d2 = dist2(p1, v[j]);
            if (d2 > maxDist2) {
                maxDist2 = d2;
            }
        }
    }
    return sqrt(maxDist2);
}

cv::Point2f rotate2d(const cv::Point2f& inPoint, const double& angRad) {
    cv::Point2f outPoint;
    //CW rotation
    outPoint.x = std::cos(angRad)*inPoint.x - std::sin(angRad)*inPoint.y;
    outPoint.y = std::sin(angRad)*inPoint.x + std::cos(angRad)*inPoint.y;
    return outPoint;
}

cv::Point2f rotatePoint(const cv::Point2f& inPoint, const cv::Point2f& center, const double& angRad)
{
    return rotate2d(inPoint - center, angRad) + center;
}

float distFromPointToLine(const cv::Point &start, const cv::Point &end, const cv::Point &p) {
    float A = 1 / float(end.x - start.x); if (A == 0) A += 0.000001;
    float B = 1 / float(start.y - end.y); if (B == 0) B += 0.000001;
    float C = start.y / float(end.y - start.y) + start.x / float(start.x - end.x);
    float dist = abs(A * p.x + B * p.y + C) / (float)sqrt(A * A + B * B);
    return dist;
}

cv::Point symmetricPoint(const cv::Point &start, const cv::Point &end, const cv::Point &p) {
    const cv::Point &p1 = start;
    const cv::Point &p2 = end;
    const cv::Point &p0 = p;
    float dpx = (sqr(p1.x - p2.x) * p0.x + (p1.y - p2.y) * (p1.x - p2.x) * 2 * (p0.y - p1.y) + (sqr(p1.y - p2.y) * (2 * p1.x - p0.x))) / float(sqr(p1.x - p2.x) + sqr(p1.y - p2.y));
    float dpy = (dpx + p0.x - 2 * p1.x) * (p2.y - p1.y) / float(p2.x - p1.x) + 2 * p1.y;
    cv::Point psym(dpx, dpy - p0.y);
    return psym;
}