#pragma once



//optimization
interface Func {
public:
	virtual float operator()(float x) = NULL;
};

class FuncVec : public Func {
	std::vector<float> pars;
	int parNum;
public:
	
	FuncVec();
	virtual float operator()(const std::vector<float> &x) = NULL;
	float operator()(float x);
	
	void setPars(const std::vector<float> &x);
	void setPar(float x); //set value to pars[parNum]
	void setParNumForOptimization(int parNum);

	const std::vector<float>& getPars()const;
	
};

enum OptType {
	OPT_GOLDEN_SECTION,
	OPT_ITERATE, 
	OPT_ITERATE_ALL_INT, //all int values between a and b 
	OPT_GRADIENT_DECENT
};

//returns opt x
void calcOptimumOnSegment(Func &f, float &optX, float &optVal, float a, float b, 
						  bool bFindMin = true, size_t steps = 10, int optType = OPT_GOLDEN_SECTION);
float calcOptimumOnSegment(Func &f, float a, float b, 
						   bool bFindMin = true, size_t steps = 10, int optType = OPT_GOLDEN_SECTION);


float calcOptimum(Func &f, float x, bool bFindMin = true, size_t steps = 10, int optType = OPT_GRADIENT_DECENT);


enum OptTypeVec {
	OPT_SIMPLEX
};

//calc optimum of vector function using simplex method 
std::vector<float> calcOptimumVec(FuncVec &f, const std::vector<float> &x, const std::vector<float> &R,
							   const std::vector<float> &minX, const std::vector<float> &maxX,
							   const std::vector<size_t> &internalSteps,
							   bool bFindMin,
							   size_t externalSteps, 
							   int externalOptType = OPT_SIMPLEX,
							   int internalOptType = OPT_GOLDEN_SECTION,
							   int parStartNum = 0);

//calc optimum of vector function using simplex method 
std::vector<float> calcOptimumVec(FuncVec &f, const std::vector<float> &x, const std::vector<float> &R,
							   bool bFindMin,
							   size_t externalSteps, 
							   size_t internalSteps,
							   int externalOptType = OPT_SIMPLEX,
							   int internalOptType = OPT_GOLDEN_SECTION,
							   int parStartNum = 0);