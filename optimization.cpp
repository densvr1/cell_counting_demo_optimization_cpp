#include "optimization.h"


FuncVec::FuncVec() {
}

void FuncVec::setPars(const std::vector<float> &x) {
	this->pars = x;
}

void FuncVec::setPar(float x) {
	pars[parNum] = x;	
}

void FuncVec::setParNumForOptimization(int parNum) {
	this->parNum = parNum;
}

float FuncVec::operator()(float x) {
	pars[parNum] = x;
	return operator()(pars);	
}

const std::vector<float>& FuncVec::getPars()const {
	return pars;
}


//golden section
void calcOptimumOnSegment(Func &f, float &optX, float &optVal, float a, float b, bool bFindMin, size_t steps, int optType) { 
	switch (optType) {
	case OPT_GOLDEN_SECTION:
	{
		float x1, x2, fu1, fu2;	
		x1 = a+(b-a)*(3.f-sqrt(5.f))/2.0f;
		x2 = a+(b-a)*(sqrt(5.f)-1.f)/2.0f;
		//x1 = (a + b) / 2.0;
		//x2 = (a + b) / 2.0;
		//TODO start point must be set
		fu1 = f(x1);
		fu2 = f(x2);
		for(size_t i = 0; i < steps; i++) {
			if( fu1 > fu2 && !bFindMin ||
				fu1 < fu2 && bFindMin
				) 
			{
				b = x2;
				x2 = x1;
				fu2 = fu1;
				x1 = a+(b-a)*(3.f-sqrt(5.f))/2.0f;
				fu1 = f(x1);
			} else {
				a = x1;
				x1 = x2;
				fu1 = fu2;
				x2 = a+(b-a)*(sqrt(5.f)-1.f)/2.0f;
				fu2 = f(x2);
			}
		}
		optX = (x1 + x2) / 2.0f;
		optVal = f(optX);	  
		break;	 
	}
	case OPT_ITERATE:
	{
		if (a - b == 0) {
			optX = a;
			optVal = f(a);
			return;	
		}
		float x = a;
		float step = (b - a) / (float)steps;
		float bestX = (a + b) / 2.f;
		float bestOpt = f(bestX);
		for(size_t i = 0; i <= steps; i++, x += step) {
			float curOpt = f(x);
			if (curOpt < bestOpt && bFindMin ||
				curOpt > bestOpt && !bFindMin) 
			{
				bestOpt = curOpt;
				bestX = x;
			}
		}
		optX = bestX;
		optVal = bestOpt;
		return;		   
	}
	case OPT_ITERATE_ALL_INT:
	{
		int A = int(a); 
		int B = int(b);
		float bestOpt = f(a);
		float bestX = a;
		size_t start_x = size_t(a + 1);
		size_t end_x = size_t(b);
		for(size_t x = start_x; x <= end_x; x++) {
			float opt = f(float(x));
			if( opt > bestOpt && !bFindMin ||
				opt < bestOpt && bFindMin
				)
			{
				bestOpt = opt;
				bestX = (float)x;
			}
		}
		optX = bestX;
		optVal = bestOpt;
		return;
	}
	default:
	{
		logger<<"optType "<<optType<<" not defined in void calcOptimumOnSegment(Func &f, float &optX, float &optVal, float a, float b, bool bFindMin, int steps, int optType) { "<<std::endl;
		return;	
	}
	} //end of swicth

}

//fast fix. TODO delete
float calcOptimumOnSegment(Func &f, float a, float b, bool bFindMin, size_t steps, int optType) {
	float optX = (a + b) / 2.f;
	float optVal;
	calcOptimumOnSegment(f, optX, optVal, a, b, bFindMin, steps, optType);
	return optX;
}






/*
float calcOptimum(Func &f, float a, float b, bool bFindMin, int steps, int optType) { 
	float x = a;
	float step = (b - a) / (float)steps;
	float bestX = (a + b) / 2.0;
	float bestOpt = f(bestX);
	for(size_t i = 0; i < steps; i++, x += step) {
		float curOpt = f(x);
		if (curOpt < bestOpt && bFindMin ||
			curOpt > bestOpt && !bFindMin) 
		{
			bestOpt = curOpt;
			bestX = x;
		}
	}
	return bestX;
} */

//calc optimum of vector function using simplex method 
std::vector<float> calcOptimumVec(FuncVec &f, const std::vector<float> &x, const std::vector<float> &R,
							   const std::vector<float> &minX, const std::vector<float> &maxX,
							   const std::vector<size_t> &internalSteps,
							   bool bFindMin,
							   size_t externalSteps, 
							   //int internalSteps,
							   int externalOptType,
							   int internalOptType,
							   int parStartNum)
{		
	if (!(R.size() == x.size())) {
		logger<<"assertion failed !(R.size() == x.size()) in std::vector<float> calcOptimum(FuncVec &f, const std::vector<float> &a, const std::vector<float> &b, )"<<std::endl;
		return std::vector<float>();
	}
	float eps = 0.9f;
	const size_t dim = x.size();
	f.setPars(x);
	float bestOpt = f.operator()(x);
	const size_t iterStart = parStartNum % dim;
	const size_t iterEnd = iterStart + externalSteps * dim;

	std::vector<float> oldPars;
	for(size_t i = iterStart; i < iterEnd; i++) {
		bool bDoneIter = i % dim == iterStart;
		if (bDoneIter) {
			if (i != iterStart) {
				bool bContinue = false;
				std::vector<float> curPars = f.getPars();
				for(size_t parNum = 0; parNum < dim; parNum++) {
					if (abs(curPars[parNum] - oldPars[parNum]) > eps) {
						bContinue = true;	
					}
				}
				if (!bContinue) {
					break;
				}
			}
			oldPars = f.getPars();
		}

		

		int parNum = i % dim;
		float r = R[parNum];
		int steps = internalSteps[parNum];
		if (r == 0 || steps == 0) {
			continue;
		}
		f.setParNumForOptimization(parNum);
		float X = f.getPars()[parNum];
		float min_x = max(X - r, minX[parNum]);
		float max_x = min(X + r, maxX[parNum]);

		float curPar = calcOptimumOnSegment(f, min_x, max_x, bFindMin, steps, internalOptType); 
		float curOpt = f.operator()(curPar);
		if (bFindMin && curOpt < bestOpt ||
			!bFindMin && curOpt > bestOpt)
		{
			bestOpt = curOpt;
			f.setPar(curPar);
		}
	}
	return f.getPars();
}

//calc optimum of vector function using simplex method 
std::vector<float> calcOptimumVec(FuncVec &f, const std::vector<float> &x, const std::vector<float> &R,
							   bool bFindMin,
							   size_t externalSteps, 
							   size_t internalSteps,
							   int externalOptType,
							   int internalOptType,
							   int parStartNum)
{
	std::vector<float> minX(x.size(), -INF), maxX(x.size(), INF);
	std::vector<size_t> internalStepsArr(x.size(), internalSteps);
	return calcOptimumVec(f, x, R, minX, maxX, internalStepsArr, bFindMin, externalSteps, externalOptType, internalOptType, parStartNum);
}